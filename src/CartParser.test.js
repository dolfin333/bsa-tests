import CartParser from "./CartParser";

let parser, validation, parse, createErrorObject;

beforeEach(() => {
  parser = new CartParser();
  createErrorObject = parser.createError;
});

describe("CartParser - unit tests", () => {
  it("should return an object from a parsed line", () => {
    const mockData = `Mollis consequat,9.00,2`;

    expect(parser.parseLine(mockData)).toMatchObject({
      name: "Mollis consequat",
      price: 9,
      quantity: 2,
    });
  });

  it("should throw error in parse function when validation failed", () => {
    parse = parser.parse.bind(parser);
    parser.readFile = jest.fn(
      () => `Product name,Price,Quantity
		Mollis consequat,18.90`
    );
    parser.validate = jest.fn(() => [
      createErrorObject(
        "row",
        1,
        -1,
        `Expected row to have 3 cells but received 2.`
      ),
    ]);

    expect(() => parse("")).toThrowError("Validation failed!");
  });

  describe("Validation errors occure", () => {
    beforeEach(() => {
      validation = parser.validate.bind(parser);
    });

    it("should return no errors when all data is correct", () => {
      const mockData = `Product name,Price,Quantity
	Mollis consequat,9.00,2`;

      expect(validation(mockData)).toEqual([]);
    });

    it("should return error when value in column with type 'numberPositive' is negative number", () => {
      const price = -18.9;
      const mockData = `Product name,Price,Quantity
	Mollis consequat,${price},2`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "cell",
          1,
          1,
          `Expected cell to be a positive number but received "${price}".`
        ),
      ]);
    });

    it("should return error when value in column with type 'numberPositive' is NaN", () => {
      const price = "123 ABC";
      const mockData = `Product name,Price,Quantity
	Mollis consequat,${price},2`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "cell",
          1,
          1,
          `Expected cell to be a positive number but received "${price}".`
        ),
      ]);
    });

    it("should return error when value in header has unexpected name", () => {
      const headerName = "Prie";
      const mockData = `Product name,${headerName},Quantity
	Mollis consequat,18.90,2`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "header",
          0,
          1,
          `Expected header to be named "Price" but received ${headerName}.`
        ),
      ]);
    });

    it("should return error when value in header is missed", () => {
      const mockData = `Product name, Price
	Mollis consequat,18.90,2`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "header",
          0,
          2,
          `Expected header to be named "Quantity" but received undefined.`
        ),
      ]);
    });

    it("should return error when header is missed", () => {
      const productName = "Mollis consequat";
      const price = 18.9;
      const quantity = 2;
      const mockData = `${productName},${price},${quantity}`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "header",
          0,
          0,
          `Expected header to be named "Product name" but received ${productName}.`
        ),
        createErrorObject(
          "header",
          0,
          1,
          `Expected header to be named "Price" but received ${price}.`
        ),
        createErrorObject(
          "header",
          0,
          2,
          `Expected header to be named "Quantity" but received ${quantity}.`
        ),
      ]);
    });

    it("should return error when an amount of columns is unexpected", () => {
      const mockData = `Product name,Price,Quantity
	Mollis consequat,18.90`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "row",
          1,
          -1,
          `Expected row to have 3 cells but received 2.`
        ),
      ]);
    });

    it("should return error when value in column with type 'string' is an empty string", () => {
      const mockData = `Product name,Price,Quantity
       ,18.90,1`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "cell",
          1,
          0,
          `Expected cell to be a nonempty string but received "".`
        ),
      ]);
    });

    it("should return all validation errors", () => {
      const headerName = "Prie";
      const price = -18.9;
      const mockData = `Product name,${headerName},Quantity
       ,18.90,1
			 Consectetur adipiscing,${price},10
			 Condimentum aliquet,13.90`;

      expect(validation(mockData)).toEqual([
        createErrorObject(
          "header",
          0,
          1,
          `Expected header to be named "Price" but received ${headerName}.`
        ),
        createErrorObject(
          "cell",
          1,
          0,
          `Expected cell to be a nonempty string but received "".`
        ),
        createErrorObject(
          "cell",
          2,
          1,
          `Expected cell to be a positive number but received "${price}".`
        ),
        createErrorObject(
          "row",
          3,
          -1,
          `Expected row to have 3 cells but received 2.`
        ),
      ]);
    });
  });
});

describe("CartParser - integration test", () => {
  beforeEach(() => {
    parse = parser.parse.bind(parser);
  });
  it("should return parsed data", () => {
    parser.readFile = jest.fn(
      () => `Product name,Price,Quantity
			Mollis consequat,9.00,2
			Tvoluptatem,10.32,1
			Scelerisque lacinia,18.90,1
			Consectetur adipiscing,28.72,10
			Condimentum aliquet,13.90,1`
    );

    expect(parse("")).toMatchObject({
      items: [
        {
          name: "Mollis consequat",
          price: 9,
          quantity: 2,
        },
        {
          name: "Tvoluptatem",
          price: 10.32,
          quantity: 1,
        },
        {
          name: "Scelerisque lacinia",
          price: 18.9,
          quantity: 1,
        },
        {
          name: "Consectetur adipiscing",
          price: 28.72,
          quantity: 10,
        },
        {
          name: "Condimentum aliquet",
          price: 13.9,
          quantity: 1,
        },
      ],
      total: 348.32,
    });
  });
});
